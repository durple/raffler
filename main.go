package main

import (
	"fmt"
	"math/rand"
	"sort"
	"time"
)

var nestEntries = []string{
	"entry1",
	"entry2",
	"entry3",
	"entry4",
}

var chromecastEntries = []string{
	"entry1",
	"entry2",
	"entry4",
	"entry5",
	"entry6",
	"entry7",
}

type Pair struct {
	Key   string
	Value int
}

type PairList []Pair

func (p PairList) Len() int           { return len(p) }
func (p PairList) Less(i, j int) bool { return p[i].Value < p[j].Value }
func (p PairList) Swap(i, j int)      { p[i], p[j] = p[j], p[i] }

func rankByCount(counts map[string]int) PairList {
	pl := make(PairList, len(counts))
	i := 0
	for k, v := range counts {
		pl[i] = Pair{k, v}
		i++
	}
	sort.Sort(sort.Reverse(pl))
	return pl
}

type winnerError struct {
	problem string
}

func (e *winnerError) Error() string {
	return fmt.Sprintf("%s", e.problem)
}

func getWinner(contestants []string) (string, error) {
	rand.Seed(time.Now().UTC().UnixNano())
	var contestantMap map[string]int
	contestantMap = make(map[string]int)
	// let's do this multiple times
	// pick a winner who gets the max picks in 1000 random tries
	for i := 0; i < 1000; i++ {
		contestant := contestants[rand.Intn(len(contestants))]
		if count, ok := contestantMap[contestant]; ok {
			contestantMap[contestant] = count + 1
		} else {
			contestantMap[contestant] = 1
		}
	}
	rankings := rankByCount(contestantMap)
	// Check for a tie at the top
	if rankings[0].Value == rankings[1].Value {
		return "No Winner", &winnerError{"There is a tie, please try again!"}
	}
	return rankings[0].Key, nil
}

func removeContestant(contestants []string, contestant string) []string {
	pos := 0
	index := 0
	found := false
	for _, key := range contestants {
		if key == contestant {
			index = pos
			found = true
		}
		pos++
	}
	if found {
		fmt.Println(contestants[index], " is being removed from the contestants for the next raffle")
		contestants = append(contestants[:index], contestants[index+1:]...)
	}
	return contestants
}

func addAdditionalEntries(chromecastList []string, nestList []string) []string {
	//create map of the nestList
	nestMap := make(map[string]bool)
	for _, a := range nestList {
		nestMap[a] = true
	}
	for _, contestant := range chromecastList {
		_, present := nestMap[contestant]
		if !present {
			fmt.Println("Additional entry for Chromecast for: ", contestant)
			chromecastList = append(chromecastList, contestant)
		}
	}
	return chromecastList
}

func main() {
	// get nest winner
	nestWinner, err := getWinner(nestEntries)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("Nest Winner:", nestWinner)

	// remove nest winner from the chromecast list
	chromecastEntries = removeContestant(chromecastEntries, nestWinner)
	// check chromecast list against the nest list
	// if chromecast entry is not in the nest list add additional entry to the chromecast list
	chromecastEntries = addAdditionalEntries(chromecastEntries, nestEntries)
	// get chromecast winner
	chromecastWinner, err := getWinner(chromecastEntries)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("Chromecast Winner: ", chromecastWinner)

}
