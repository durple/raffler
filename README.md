# Chromecast and Nest Raffle

Code to raffle away a chromecast and nest. Following terms apply:

- You may not sell the item you win. You may gift it to someone else. I cannot verify this ever but I will take your word for it.
- You may enter to win both the chromecast and the nest
- If you enter to win the nest and win, you cannot win the chromecast, your entry to win the chromecast will be removed.
- If you enter to win the chromecast but not the nest, you get 2 entries to win the chromecast, thus doubling your chances to win it.
